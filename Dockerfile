FROM centos:latest
MAINTAINER Umar Purnomo <umar.purnomo@gmail.com>

ENV java_version="1.8.0"

# Set the localtime
ENV localtz=Australia/Sydney
RUN rm -f /etc/localtime \
    && ln -s /usr/share/zoneinfo/${localtz}

# Update OS
RUN yum -y update && yum -y clean all
RUN yum -y install java-${java_version}-openjdk-devel maven

RUN mkdir -p /tmp/src
COPY src /tmp/src
COPY pom.xml /tmp

RUN cd /tmp; \
  ls -lR; \
  mvn clean; \
  mvn package; \
  ls -lR target

EXPOSE 8080
CMD ["java", "-jar", "/tmp/target/api-status-v1-0.0.1.jar"]