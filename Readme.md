# Spring Boot template used for API development
A simple Springboot application to demonstrate API development. 
This application provides one GET request and responses with a simple message.
### Install
```
mvn package
```
### Run
```.env
java -jar <jarfile.jar>
```