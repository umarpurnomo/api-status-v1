package me.geekable.status.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextListener implements ApplicationListener<ServletWebServerInitializedEvent> {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationContextListener.class);

    @Override
    public void onApplicationEvent(ServletWebServerInitializedEvent servletWebServerInitializedEvent) {
        int springRandomPort = servletWebServerInitializedEvent.getApplicationContext().getWebServer().getPort();
        logger.info("***************************************************************");
        logger.info("*******");
        logger.info("*******       Listening on Port " + springRandomPort);
        logger.info("*******");
        logger.info("***************************************************************");
    }
}
