package me.geekable.status.controller;

import lombok.extern.slf4j.Slf4j;
import me.geekable.status.model.StatusResponseModel;
import me.geekable.status.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class StatusApiController implements StatusApi {

    @Autowired
    StatusService statusService;

    @Override
    public StatusResponseModel getStatus(String message) {
        return statusService.getStatus(message);
    }
}
