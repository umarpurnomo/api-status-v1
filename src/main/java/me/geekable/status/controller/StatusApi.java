package me.geekable.status.controller;

import me.geekable.status.model.StatusResponseModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

// requires io.swagger.annotations
//@Api(value = "status", description = "Status API endpoint")
@RequestMapping("${server.servlet.apiVersion}")
public interface StatusApi  {


    @RequestMapping(value = "${server.servlet.statusPath}", produces = "application/json", method = RequestMethod.GET)
    StatusResponseModel getStatus(String message);


}
