package me.geekable.status;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.retry.annotation.EnableRetry;


@SpringBootApplication
@EnableScheduling
@EnableRetry
public class ServiceManager {

  public static void main(String[] args) {
    final SpringApplication springApplication = 
        new SpringApplication(ServiceManager.class);
      springApplication.addListeners(new ApplicationPidFileWriter());
      springApplication.run(args);

  }
}
